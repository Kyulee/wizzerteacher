export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "wizzerteacherd7c98a88": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        },
        "userPoolGroups": {
            "eleveGroupRole": "string"
        }
    }
}