import React from "react";
import { Button } from "react-bootstrap";
import logo from "../../assets/wizzerT.png";
import { useNavigate } from "react-router-dom";

export default function Welcome() {
  const history = useNavigate();

  const loginPage = () => {
    history("/login");
  };
  const registerPage = () => {
    history("/signup");
  };
  return (
    <div>
      <img src={logo} alt="logoWizzerT" />
      <p>
        La plateforme pour apprendre les notions de bases pour etre développeur
        ou UI/UX designer.
      </p>
      <Button variant="primary" onClick={loginPage}>
        Se connecter
      </Button>
      <Button variant="primary" onClick={registerPage}>
        S'inscrire
      </Button>
    </div>
  );
}
