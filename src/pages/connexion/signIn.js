import React from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../../assets/wizzerT.png";
import { useNavigate } from "react-router-dom";

export default function signIn() {
  const history = useNavigate();

  const homePage = () => {
    history("/dashboardE");
  };
  return (
    <div>
      <img src={logo} alt="logoWizzerT" />
      <h2 style={{ color: "#4B6DC4" }}>ME CONNECTER</h2>
      <Form
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Email" />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Mot de passe</Form.Label>
          <Form.Control type="password" placeholder="Mot de passe" />
        </Form.Group>
        <Button variant="primary" onClick={homePage}>
          Se connecter
        </Button>
      </Form>
      <Link to="/signup">Je n'ai pas de compte</Link>
    </div>
  );
}
