import React from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../../assets/wizzerT.png";

export default function signUp() {
  return (
    <div>
      <img src={logo} alt="logoWizzerT" />
      <h2 style={{ color: "#4B6DC4" }}>M'INSCRIRE</h2>
      <Form
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Email" />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Mot de passe</Form.Label>
          <Form.Control type="password" placeholder="Mot de passe" />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Confirmation Mot de passe</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirmation du mot de passe"
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          M'inscrire
        </Button>
      </Form>
      <Link to="/login">Je n'ai déja un compte</Link>
    </div>
  );
}
