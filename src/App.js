import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import Login from "./pages/connexion/signIn";
import Register from "./pages/connexion/signUp";
import Welcome from "./pages/connexion/Welcome";
import DashboardE from "./pages/dashboardEleve/dashboardE";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/signup" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="dashboardE" element={<DashboardE />} />
        <Route exact path="/" element={<Welcome />} />
      </Routes>
    </div>
  );
}

export default App;
